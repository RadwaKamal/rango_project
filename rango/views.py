from django.shortcuts import render
from django.http import HttpResponse 

context_dict = {'boldmessage': "Crunchy, creamy, cookie, candy, cupcake!"}
about_context = {'name' : "rkamal"}

def index(request):
  return render(request, 'rango/index.html', context = context_dict)

def about(request):
  return render(request, 'rango/about.html', context = about_context)
  